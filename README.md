<h1>Optimus Sitemap Generator</h1>
<p>Optimus Sitemap Generator (OSG) is a universal XML sitemap generator that works by crawling your website, avoiding excessive bandwidth overhead by only scanning the contents of pages that have changed since the last time the sitemap was generated.</p>
<h3>Download</h3>
<table style="height: 82px; width: 480px;">
<thead>
<tr style="height: 18px;">
<th style="height: 18px; width: 118.517px;">Platform</th>
<th style="height: 18px; width: 108.483px;">Version</th>
<th style="height: 18px; width: 158.7px;">Package</th>
<th style="height: 18px; width: 66.3px;">Size</th>
</tr>
</thead>
<tbody>
<tr style="height: 18px;">
<td style="height: 18px; width: 118.517px;">Linux (32-bit)</td>
<td style="height: 18px; width: 108.483px;">1.1 &ndash; 2019-10-06</td>
<td style="height: 18px; width: 158.7px;"><a href="https://gitlab.com/anarcho-copy/osg/-/raw/master/linux-i386.tar.gz">linux-i386.tar.gz</a></td>
<td style="height: 18px; width: 66.3px;">3.5 MB</td>
</tr>
<tr style="height: 10px;">
<td style="height: 10px; width: 118.517px;">Linux (64-bit)</td>
<td style="height: 10px; width: 108.483px;">1.1 &ndash; 2019-10-06</td>
<td style="height: 10px; width: 158.7px;"><a href="https://gitlab.com/anarcho-copy/osg/-/raw/master/osg">osg</a></td>
<td style="height: 10px; width: 66.3px;">6.8 MB</td>
</tr>
<tr style="height: 18px;">
<td style="height: 18px; width: 118.517px;">Android (arm64)</td>
<td style="height: 18px; width: 108.483px;">1.1 &ndash; 2019-10-06</td>
<td style="height: 18px; width: 158.7px;"><a href="https://gitlab.com/anarcho-copy/osg/-/raw/master/android-arm64.tar.gz">android-arm64.tar.gz</a></td>
<td style="height: 18px; width: 66.3px;">3.6 MB</td>
</tr>
<tr style="height: 18px;">
<td style="width: 118.517px; height: 18px;">&nbsp;</td>
<td style="width: 108.483px; height: 18px;">Original Repo</td>
<td style="width: 158.7px; height: 18px;"><a href="https://github.com/rpendleton/osg">github.com/rpendleton/osg</a></td>
<td style="width: 66.3px; height: 18px;">&nbsp;</td>
</tr>
</tbody>
</table>
<h3>Usage Examples</h3>
<table>
<thead>
<tr>
<th>Command</th>
<th>Explanation</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>./osg sitemap.xml example.com</code></td>
<td>Generate sitemap.xml by crawling the pages on example.com</td>
</tr>
<tr>
<td><code>./osg sitemap.xml.gz example.com/science/</code></td>
<td>Generate sitemap.xml.gz by crawling the pages linked to on example.com/science/</td>
</tr>
<tr>
<td><code>./osg -c 10 sitemap.xml a.com b.com</code></td>
<td>Generate sitemap.xml by crawling the pages linked to on a.com and b.com, crawling up to 10 pages at once</td>
</tr>
<tr>
<td><code>./osg -v sitemap.xml example.com</code></td>
<td>Generate sitemap.xml by crawling the pages linked to on example.com, showing additional information about the crawling process</td>
</tr>
</tbody>
</table>
<p>Run <code>./osg</code> without any parameters to get an overview and explanation of all available options.</p>
<h3>Features</h3>
<ul>
<li>Crawls pages only once, even if they&rsquo;re linked to from many pages</li>
<li>Reads existing sitemap and avoids scanning pages that haven&rsquo;t been updated</li>
<li>Crawling an arbitrary number of URLs simultaneously</li>
<li>HTTP KeepAlive and session re-use (when applicable)</li>
<li>Warns if any pages on your site don&rsquo;t load, return a 404 Not Found status, etc.</li>
<li>Customizable User-Agent (<code>&ndash;ua</code> flag)</li>
</ul>
<h3>FAQ</h3>
<p><strong>Q:</strong> How do I install and use OSG?<br /><strong>A:</strong> On Windows, download and extract the zip file above, then run osg.exe either through a command prompt, or by right-clicking osg.exe, making a shortcut, changing the parameters for that shortcut (in Properties) to e.g.: <code>&ldquo;D:.exe&rdquo; -c 10 sitemap.xml example.com</code>, and then running it. On Linux, copy the link for your architecture above, then run <code>curl -s &lt;link&gt; | tar xvz</code>, and you&rsquo;re good to go. <code>cd osg</code>, and run e.g. <code>./osg -c 10 sitemap.xml example.com</code>.</p>
<p><strong>Q:</strong> Do I need to have WordPress, W3 Total Cache, WP Super Cache, memcached, &hellip; to use OSG?<br /><strong>A:</strong> No. OSG is completely indifferent to what powers your website. As long as there are links on the site, OSG can generate a sitemap for it.</p>
<p><strong>Q:</strong> How do I generate an XML sitemap regularly?<br /><strong>A:</strong> The easiest way is to set up a cron job. On most Linux distributions you can do this by adding a cron entry using <code>crontab -e</code>. The entry can be e.g. <code><em>/5 </em> * * * /home/patrick/osg /home/patrick/sitemap.xml example.com</code>, which will run OSG every five minutes. For more information, see Ubuntu&rsquo;s <a title="Cron How To" href="https://help.ubuntu.com/community/CronHowto">Cron Howto</a>.</p>
<p>(Note that Cron&rsquo;s environment/path is very minimal, and you might need to use full paths to your commands.)</p>
<h3>License</h3>
<p>Optimus Sitemap Generator (OSG) is released under the MIT license.</p>
